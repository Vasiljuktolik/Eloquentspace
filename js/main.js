'use strict';

// todo не понятно что это за файл в дальнейшем если он не будет использоваться то удалить

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FrondevoMain = function () {
    function FrondevoMain() {
        _classCallCheck(this, FrondevoMain);

        //this.topNav = $('nav.h-menu-dd').eq(0);

        this._eventHandlersInit();
    }

    _createClass(FrondevoMain, [{
        key: '_modulesInit',
        value: function _modulesInit() {
            //let scrollToTop = new FrondevoScrollToTop();
            //let topNav = new FrondevoTopNav( this.topNav );
        }
    }, {
        key: '_eventHandlersInit',
        value: function _eventHandlersInit() {
            var _this = this;

            $(document).on('ready', function () {
                return _this._modulesInit();
            });
        }
    }]);

    return FrondevoMain;
}();

var main = new FrondevoMain();